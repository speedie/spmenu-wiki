Using spmenu on macOS
=====================
**NOTE: This article features information that may not apply to newer spmenu versions. An update to this article needs to be made.**

spmenu (sort of) supports macOS. This is done through an X11 implementation for macOS called [XQuartz](https://www.xquartz.org/). It requires some extra effort however.

# Install Homebrew

Start by installing Homebrew (the package manager) on your Mac. From there run: `brew install libxft libxinerama xquartz libconfig libx11 freetype`. Make sure Xquartz is set up and functional before proceeding.

# Editing the buildconf.

**NOTE: The following instructions only apply to users of spmenu 1.0 and greater. Previous versions require a significant amount of work to get running.**

The buildconf is just a shell script that the build script (build.sh) reads. So if we want to append any options we simply edit this file.
First we need to set `PREFIX` to `/usr/local` instead of the default `/usr`. This is because Apple as of macOS 10.15 Catalina no longer allows us to write to `/usr/bin` and `/usr/share`. This avoids any `Permission denied` errors you may get (yes, even with sudo)

Now we need to set `pango=false`, `pangoxft=false`, `imlib2=false` and `openssl=false`. Currently, these features are **not supported** on macOS. [Should've installed a better operating system, bud.](https://archlinux.org)

# Compiling and installing

Now that we've edited our buildconf, we can simply run `sudo ./build.sh`.

# Notes

- Image support and Pango markup will not work, because spmenu was not compiled with those. It *is* possible to compile spmenu with support for this but it is not easy for the beginner, for various reasons.
- spmenu_run is non-functional, and it will warn you about this during runtime on 1.0 and greater.
- Profile menu does not work, it does absolutely nothing.
