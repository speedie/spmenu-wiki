Using spmenu as a run launcher
==============================

spmenu provides an extensible Bash script called `spmenu_run`. This program handles listing of binaries in $PATH (all paths where binaries can be located), .desktop entries, file listing, and more. While it *is* just a Bash script, this is an advantage for those who like to extensively customize their run launcher. If you want to learn more about this, see [[Functions in spmenu_run]].

When running it for the first time, you should get a helpful message telling you how it works, because chances are most spmenu users are using it for the run launcher component. After that, by default $PATH will be used. The default behaviour can be set in the configuration file, `~/.config/spmenu/run/config`, which should be created when you run spmenu_run for the first time.

spmenu_run unlike dmenu_run does **not** pass arguments to the menu, because it uses arguments for itself. If you want to pass arguments to spmenu, you use the `-a` flag or `--args`. For example to set the prompt to `Run:`, you would do `spmenu_run --args "--prompt Run:"`. spmenu_run accepts more arguments, and you can see all of them by running `spmenu_run -h`. Of course, you can see spmenu arguments by running `spmenu -h`.

### General launcher

The normal, default run launcher mode lists out executables in $PATH. You can change the sorting of these items, as well as prevent duplicates. You can even choose to whitelist/blacklist certain entries in your config file.

As if this wasn't enough, if an awesome URL such as https://speedie.site is typed in, it will be opened in the user's default web browser. It should be noted that this also works with magnet links. You can also add your own. See [[Functions in spmenu_run]] for that.

If you prepend `#`, the selected item will be ran in the user's default terminal emulator instead of just a shell. For example, if you want to open `vim` you can just type in `#vim`. You can also prepend `?`, which will open the man page for the selected item. For example `?spmenu` will open the man page for spmenu. By default the man page will be displayed in spmenu, which is very useful. You can (of course) customize this. Typing in `?` alone will display the help you saw the first time you used spmenu_run.

### Desktop launcher

spmenu_run supports .desktop entries too. To use it, simply pass the `-d` argument to spmenu_run. By default it should take quite some time as it caches entries. To speed things up, arrays are used.

If `IMAGE=true` (default), images will also be parsed and displayed. You can set the directories spmenu will look for images in by overriding `ICON_DIR` in the config file. When an item is selected, the command will run as usual.

Note that when you want to display new entries, you need to refresh the cache.

### File manager

spmenu_run provides a basic file manager. To use it, pass the `-f` argument to spmenu_run. If you wish, you can also specify a path after. It doesn't actually do anything except list out files and directories, change directories and open files. However its very useful in shell scripting. The entries are (by default) colorized, as the standard `ls` command supports this. You can pass extra arguments to `ls` by overriding `LS_ARGS` in your config.

If `--stdout` is passed, the last selected **file** is output to standard output. Otherwise the selected file will be opened in an appropriate program determined by the file extension.

### Standard output

If you're going to be using spmenu_run to script, it may be useful to output the selected item to standard output rather than handling it directly. To enable this you can simply pass the `-o` flag or `--stdout` if you prefer. For example, to set a wallpaper you could do something like this:

`spmenu_run -fm ~/Wallpapers --stdout | xargs xwallpaper --stretch`

### Binding keys

speedwm uses spmenu by default as of 1.9, for other window manager you'll want to bind a keybind. Doing so is out of scope for this article though, check your window manager's documentation for this.

### More customization

For more advanced customization, you may want to consider using functions. It's not too difficult, especially if you're familiar with shell scripting. See [[Functions in spmenu_run]] for more information.