User themes
===========

spmenu has since version 1.1 supported user themes. Themes override the overall design of spmenu, through for example defining colors. Version 0.3 through 1.0 supported profiles. Profiles are no longer supported in spmenu, but you can still find the old profiles [here](https://git.speedie.site/spmenu-themes).

This wiki article is meant to be a list of themes for spmenu. Users may edit this article to add their own profiles with credit given of course, and users may download others' profiles to use with spmenu.

You may also want to see [[Theme managers]] for a list of theme managers that can be used to install these. If you do not want to use a theme manager, simply copy the theme you want to use to `~/.config/spmenu/theme.conf`

Tip: Use `Ctrl+f` to search through the page.

### Rules for editors

- Must be licensed under a free (as in freedom) license.
- No duplicates, we don't need multiple versions of Nord for example.
- Please try to use stable hosting if possible for links. If this is not possible, consider [emailing me](mailto:speedie@speedie.site) so I can host it on my site.
- Outdated profiles may be removed if necessary. Please try to maintain it, if you don't have time to do so a disclaimer would be nice.

### Official themes

![image](/pages/images/arc-dark.png)

- [Arc Dark](https://git.speedie.site/spmenu-themes/plain/Arc-Dark.conf)
  - Author: [speedie](https://speedie.site) ([speedie@speedie.site](mailto:speedie@speedie.site))
  - This profile implements the [Arc Dark theme](https://github.com/arc-design/arc-theme) into spmenu.

![image](/pages/images/catppucino.png)

- [Catppuccin](https://git.speedie.site/spmenu-themes/plain/Catppuccin.conf)
  - Author: [speedie](https://speedie.site) ([speedie@speedie.site](mailto:speedie@speedie.site))
  - This profile implements the [Catppuccin theme](https://github.com/catppuccin/catppuccin) into spmenu which all furries seem to like.

![image](/pages/images/cyberpunk.png)

- [Cyberpunk Neon](https://git.speedie.site/spmenu-themes/plain/Cyberpunk-Neon.conf)
  - Author: [speedie](https://speedie.site) ([speedie@speedie.site](mailto:speedie@speedie.site))
  - This profile implements the [Cyberpunk Neon colorscheme](https://github.com/Roboron3042/Cyberpunk-Neon) into spmenu. This was partially taken from the [st patch](https://st.suckless.org/patches/cyberpunk-neon/), credit where credit is due.

![image](/pages/images/doom.png)

- [Doom One](https://git.speedie.site/spmenu-themes/plain/Doom-One.conf)
  - Author: [speedie](https://speedie.site) ([speedie@speedie.site](mailto:speedie@speedie.site))
  - This profile implements the [Doom One](https://github.com/doomemacs/themes) colorscheme into spmenu. For those of you that like DistroTube's dmenu, this should get you close to that look.

![image](/pages/images/dracula.png)

- [Dracula](https://git.speedie.site/spmenu-themes/plain/Dracula.conf)
  - Author: [speedie](https://speedie.site) ([speedie@speedie.site](mailto:speedie@speedie.site))
  - This profile implements the [Dracula](https://draculatheme.com/) colorscheme into spmenu.

![image](/pages/images/gruvbox-dark.png)

- [Gruvbox (Dark)](https://git.speedie.site/spmenu-themes/plain/Gruvbox-Dark.conf)
  - Author: [speedie](https://speedie.site) ([speedie@speedie.site](mailto:speedie@speedie.site))
  - This profile implements the [Gruvbox Dark](https://github.com/morhetz/gruvbox) colorscheme into spmenu.

![image](/pages/images/gruvbox-light.png)

- [Gruvbox (Light)](https://git.speedie.site/spmenu-themes/plain/Gruvbox-Light.conf)
  - Author: [speedie](https://speedie.site) ([speedie@speedie.site](mailto:speedie@speedie.site))
  - This profile implements the [Gruvbox Light](https://github.com/morhetz/gruvbox) colorscheme into spmenu.

![image](/pages/images/gruvbox-material.png)

- [Gruvbox Material](https://git.speedie.site/spmenu-themes/plain/Gruvbox-Material.conf)
  - Author: [speedie](https://speedie.site) ([speedie@speedie.site](mailto:speedie@speedie.site))
  - This profile implements the [Gruvbox Material](https://github.com/sainnhe/gruvbox-material) colorscheme into spmenu.

- [Disable global colors](https://git.speedie.site/spmenu-themes/plain/No-Global-Colors.conf)
  - Author: [speedie](https://speedie.site) ([speedie@speedie.site](mailto:speedie@speedie.site))
  - This profile disables global colors, meaning programs like Pywal will not function with spmenu. This is nice if you want to use Pywal everywhere except spmenu.

![image](/pages/images/nord.png)

- [Nord](https://git.speedie.site/spmenu-themes/plain/Nord.conf)
  - Author: [speedie](https://speedie.site) ([speedie@speedie.site](mailto:speedie@speedie.site))
  - This profile implements the [Nord](https://www.nordtheme.com/) colorscheme into spmenu.

![image](/pages/images/solarized-dark.png)

- [Solarized (Dark)](https://git.speedie.site/spmenu-themes/plain/Solarized-Dark.conf)
  - Author: [speedie](https://speedie.site) ([speedie@speedie.site](mailto:speedie@speedie.site))
  - This profile implements the [Solarized](https://ethanschoonover.com/solarized/) colorscheme into spmenu. Specifically the 'Dark' variant.

![image](/pages/images/solarized-light.png)

- [Solarized (Light)](https://git.speedie.site/spmenu-themes/plain/Solarized-Light.conf)
  - Author: [speedie](https://speedie.site) ([speedie@speedie.site](mailto:speedie@speedie.site))
  - This profile implements the [Solarized](https://ethanschoonover.com/solarized/) colorscheme into spmenu. Specifically the 'Light' variant.

![image](/pages/images/tokyo-night.png)

- [Tokyo Night](https://git.speedie.site/spmenu-themes/plain/Tokyo-Night.conf)
  - Author: [speedie](https://speedie.site) ([speedie@speedie.site](mailto:speedie@speedie.site))
  - This profile implements the [Tokyo Night](https://github.com/enkia/tokyo-night-vscode-theme) colorscheme into spmenu, ported from the VS Code theme.

- [dmenu](https://git.speedie.site/spmenu-themes/plain/dmenu.conf)
  - Author: [speedie](https://speedie.site) ([speedie@speedie.site](mailto:speedie@speedie.site))
  - This config tries to match dmenu as closely as possible, even stripping away spmenu only features to an extent.

- [Default](https://git.speedie.site/spmenu/plain/docs/spmenu.conf)
  - Author: [speedie](https://speedie.site) ([speedie@speedie.site](mailto:speedie@speedie.site))
  - This config is the default configuration for spmenu. This is automatically installed to `/usr/share/spmenu/spmenu.conf` so you *usually* don't need a copy of this.

### User-submitted themes
