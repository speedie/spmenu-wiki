Theme managers
==============

**NOTE: This article only applies to spmenu 1.1 and later. Users of spmenu 1.0 and earlier should first upgrade, or alternatively use the regular profiles.**

This article is intended to be a list of theme managers that can be installed and used. spmenu does not decide what theme manager is used, spmenu simply runs `spmenu_profile --spmenu-set-profile` when the keybind for setting a profile is activated. This means you must parse this in a separate program. See [spmenuify](https://git.speedie.site/spmenuify) for an example on how to do this.

# Rules for editors

- The theme managers must all be licensed under a free/libre license, preferably but not necessarily a copyleft license.
- Please try to use stable hosting if possible for links. If this is not possible, consider emailing me so I can host it on my site.
- Any programming language. Go wild.
- No malware/software with antifeatures.

# Official theme managers

- [spmenuify](https://git.speedie.site/spmenuify)
  - Author: [speedie](https://speedie.site) ([speedie@speedie.site](mailto:speedie@speedie.site))
  - spmenuify is the official theme manager for spmenu. It allows installation, previewing, enabling and disabling of themes.

# User theme managers