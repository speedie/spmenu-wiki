A list of spmenu scripts
========================

This article is for various scripts written with spmenu. Anyone may add their scripts to this list, provided they serve some use and are free/libre as in freedom. Additionally, please **specify any possible antifeatures and network requests made and the license.**

## Useful

- [speedwm-extras](https://git.speedie.site/speedwm-extras)
  - Author: [speedie](https://speedie.site) ([speedie@speedie.site](mailto:speedie@speedie.site))
  - License: GNU General Public License version 3.0
  - Collection of scripts for things like WLAN, Bluetooth, Wallpaper setting, audio management, screen recording and more. Designed with speedwm in mind, but can be used with any window manager or setup.
- [clipmenu-spmenu](https://github.com/speediegq/clipmenu-spmenu)
  - Author: [Chris Down](https://github.com/cdown), [speedie](https://speedie.site) ([speedie@speedie.site](mailto:speedie@speedie.site))
  - License: Public domain
  - Fork of clipmenu, now supports spmenu specific features a little better. clipmenu allows you to navigate and view past history. History is collected through a daemon which should be autostarted in your window manager config.
- [iron-spmenu](https://codeberg.org/speedie/iron-spmenu)
  - Author: [speedie](https://speedie.site) ([speedie@speedie.site](mailto:speedie@speedie.site))
  - License: GNU General Public License version 3.0
  - Fork of the [iron rautafarmi client](https://codeberg.org/speedie/iron), now displays messages and images using spmenu. Depends on `jq`, `curl`, `sed` and a POSIX compliant shell (no, not fish). Supports sending messages using the -s (send) argument.

## Show-off

- [spmenu-test](https://git.speedie.site/spmenu)
  - Author: [speedie](https://speedie.site) ([speedie@speedie.site](mailto:speedie@speedie.site))
  - License: MIT
  - Script bundled with spmenu, used to test various functionality. When making colorschemes, I highly recommend taking a screenshot of this as it allows you to view every color.
- [spmenu_sweden.sh](https://git.speedie.site/spmenu/plain/scripts/examples/spmenu_sweden.sh)
  - Author: [speedie](https://speedie.site) ([speedie@speedie.site](mailto:speedie@speedie.site))
  - License: MIT
  - Shell script written with spmenu SGR sequence support in mind. Simply recreates the flag of Sweden with blue and yellow background SGR sequences.