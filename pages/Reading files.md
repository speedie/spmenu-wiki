Reading files
=============

**NOTE: The following information only applies to spmenu 1.1 and later.**

spmenu is designed to read files. Most of the time it reads standard input which is also a file (/dev/stdin which is a symlink to /proc/self/fd/0 on systems using the Linux kernel). You don't necessarily have to read standard input or stdin though, as of spmenu 1.1 you can read any file you want.

Reading from a file has benefits. When reading from standard input the entries are only loaded once. When reading from a file however, if the file is modified the new entries will be loaded and displayed on the next X11 event.

## Reading from a file

To read from any file you want, simply run spmenu with the `-lf` or `--list-file` argument. You can actually read from standard output using `--list-file` as well, by using `stdin | spmenu --list-file /dev/stdin`, but there is really no reason to do this as reading standard input is the default spmenu behavior anyway.

For instance, `spmenu --list-file /tmp/file` would load entries from /tmp/file.

## Real world use case

Let's say we want to display the time and update it every second. In that case we can write a script like this:

```
#!/bin/sh
func() {
    while true; do
        date > /tmp/date-file
        sleep 1
    done
}

func &
spmenu -lf /tmp/date-file -p "The time is:"
```

[speedwm-extras](https://git.speedie.site/speedwm-extras)' speedwm-btctrl uses this feature to load Bluetooth entries every 5 seconds.