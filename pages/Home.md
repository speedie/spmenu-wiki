Welcome to the spmenu wiki!
---------------------------
![preview image](/pages/images/preview.png)

[spmenu](https://git.speedie.site/spmenu) is an X11 menu application based on
[dmenu](https://tools.suckless.org/dmenu) which takes standard input, parses
it, and lets the user choose an option and sends the
selected option to standard output. 

Options are separated by newlines. In addition to this, it also serves as a run launcher through the included shell script `spmenu_run` which supports running binaries from $PATH but also file management and .desktop entries.

What are you waiting for? Check it out!

It is recommended that you start out by following [[Getting Started]]. Then, you may be interested in [[Using spmenu as a run launcher]].

spmenu git repository: `https://git.speedie.site/spmenu`