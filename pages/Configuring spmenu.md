Configuring spmenu
==================
A fully complete configuration file is available in /usr/share/spmenu/spmenu.conf. This config is more or less identical to the default options. This makes it an excellent place to start because everything is already defined, you just make the changes you want.
To use this config, simply copy it to ~/.config/spmenu/spmenu.conf and restart spmenu. It should be used immediately.

What may be useful to know is that keybinds in your spmenu.conf (by default) override **all** hardcoded keybindings. This is because
otherwise keybindings can conflict or be executed twice. This creates a problem, because if a new function is implemented the user must manually add it to their config or restore the default config. This is the case with the fullscreen image feature.