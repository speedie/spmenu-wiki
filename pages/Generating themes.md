Generating themes
=================

As of 05/19/23 spmenu comes with a script called `generate-theme.sh`. As the name implies, this script generates a theme. Specifically it generates a theme from 18 different colors.

The script is located in `scripts/make/` and is **not** installed with spmenu. This is a separate tool. If you run `scripts/make/generate-theme.sh` you will immediately get a.. admittedly not that useful listing of arguments as well as how to use it to generate a theme.

Creating a color list is the first step. This can be any file. For this article we're going to assume the file is `./colors`. In there you want to set 18 different colors. The first 16 are going to be set in the same order you set terminal colors. For the two last colors, color #17 is the foreground color and color #18 is the background color. Note that colors may be reused.

Now that we have a color list, we can start generating a theme. To do this, run `scripts/make/generate-theme.sh -c ./colors` where obviously colors is the color list. This will output directly to standard output, so you may want to redirect this into a file. For testing, I usually do something like `scripts/make/generate-theme.sh -c ./colors > scheme && spmenu_test -tm ./scheme`.

It should be noted that the script doesn't provide an easy way to move around colors just yet, so it is assumed that you have a basic level of shell scripting knowledge here if you want to do that. If colors are incorrectly defined in your color list, the color will be set to #000000 as a default. Unless #000000 is part of your colorscheme, this is a good indication something is wrong.

Now that we have a proper theme, we can use it. If you're using [spmenuify](https://git.speedie.site/spmenuify) you can simply choose 'Install' and navigate to the scheme you output.
