Getting Started
===============

- [[Home]]
- [[Where do I go?]]
- [[Getting Started]]

Basics
======

- [[Using spmenu as a run launcher]]
- [[User scripts]]
- [[Configuring spmenu]]
- [[User themes]]

Advanced
========

- [[Scripting with spmenu]]
- [[Functions in spmenu_run]]
- [[Extensive code documentation]]

Frequently asked questions
==========================

- [[Wayland support]]