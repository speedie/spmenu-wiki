Functions in spmenu_run
=======================

spmenu_run has a configuration file located in ~/.config/spmenu/run/config, which should be generated when you run it for the first time. This file is simply sourced by spmenu_run meaning all functions, variables and arrays are imported. The script looks for a few functions which will run if found during different stages. There are a few of these functions, and the default config file should list them out.

- 'run_pre_func' before spawning spmenu.
- 'run_post_func' after spawning spmenu, selected item passed as an argument.
- 'desktop_pre_func' before spawning spmenu.
- 'desktop_post_func' after spawning spmenu, selected item passed as an argument.
- 'fm_pre_func' before spawning spmenu.
- 'fm_post_func' after spawning spmenu, selected item passed as an argument.
- 'fm_pre_list_func' right before listing out files.
- 'fm_post_list_func' right after listing out files.
- 'fm_dir_func' before changing directory to the selected directory.
- 'fm_line_func' for each line in ls output, the line is passed as an argument, including SGR colors.
- 'read_man' when reading a man page, selected item passed as an argument.

These are quite self-explanatory. The default configuration file even gives an example of how this can be used. Additionally the default config file should already have a 'read_man' function. It looks something like this:

    read_man() {
        man "$1" | col -b | ${RUNLAUNCHER:-spmenu} --lines 40 --columns 1 -p "man $1" 
    }

In case you are new to shell scripting, $1 here is the first argument passed to this function, outside of the function name itself which is $0. This function gets passed the input minus the '?' character, and then looks up the man page for that. Then it will parse the output and display it in an spmenu prompt. Same goes for many of the other functions. 

## Writing a function

Let's say we want to display a few more entries in the run launcher. Let's say we want to open Discord and Element in our web browser (Chromium) as an app, but don't want to have a shell script for it. Well, then we can do something like this.

    run_pre_func() {
        printf "discord\nelement\n"
    }

    run_post_func() {
        case "$1" in
            "discord") chromium --app="https://discord.com/channels/@me"; exit 0 ;;
            "element") chromium --app="https://app.element.io/index.html"; exit 0 ;;
        esac
    }

So as you can see, run_pre_func is useful if we want to print stuff. We can then use run_post_func to parse the user input. Entries from run_pre_func should be sorted before being displayed to the user, provided SORT=true in the config.

**NOTE: Do not forget to exit in the case statement, otherwise spmenu will attempt to run the input. spmenu does not exit after running the function.**

Now let's do one for the file launcher provided by spmenu_run -fm. For this example, let's open the file depending on the file extension. A pre_func is not necessary here.

    fm_post_func() {
        case "$1" in
          *.pdf) zathura "$1"; exit 0 ;; # PDFs are opened in zathura
          *.md|*.php|*.txt|*.c|*.h|*.css|*.conf) nvim "$1"; exit 0 ;; # various text files are opened in neovim
          *.html|*.htm) chromium "$1"; exit 0 ;; # html and htm is opened in chromium
          *.png|*.jpg|*.jpeg|*.gif|*.tiff) nsxiv "$1"; exit 0 ;; # images are displayed in nsxiv
          *.mp4|*.mkv|*.mp3|*.flac|*.wav) mpv "$1"; exit 0 ;; # music and videos are opened in mpv
          *) [ ! -x "$1" ] && nvim "$1"; exit 0 ;; # everything else is treated as text as long as its not executable (most likely a binary)
        esac
    }

This function will handle opening of files based on the file extension. Case statements are very useful and powerful for things like matching strings. Again, do not forget to exit or else spmenu_run will try to handle running it. Also note that the --stdout flag will **not** run this function.

Another one for the file launcher. This one however **requires** spmenu 1.0. This function will display images if possible.

    fm_line_func() {
      case "$1" in
          *.png|*.jpg|*.jpeg|*.svg|*.gif|*.tiff) printf "IMG:$1\t" ;;
      esac
    }

Note that this may add a lot of slowdown in some cases. Use with care, and feel free to adjust.
