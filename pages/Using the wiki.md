Using the wiki
==============

Lost? If you don't know where to go, you can click the icon on the top bar. The `All` button allows you to get a view of all articles that have been posted on the wiki.

There is also a search bar at the top of every page which allows you to search for any article that has been posted.

## Basic wiki features

The wiki, based on a fork of w2wiki allows you to create new articles, edit existing articles, read existing articles (that's pretty much given), [host it yourself](https://codeberg.org/speedie/speedwm-wiki), and delete articles.

Now, please note that I am giving you this power, and with great power comes great  responsibility. I want everyone to be able to help with the project. However, if it gets abused by people then I will have to remove this functionality and go back to using purely Git.

So please don't ruin it for everyone. If the wiki has to be shut down due to your vandalism, we hate you forever. That said, we do make Git backups fairly often (I will not say how often to prevent vandalism), so if an article is deleted it should be possible to restore it with minor changes lost.

## The sidebar

The sidebar is a markdown page like any other and can also be edited as any other. It is supposed to be used as a list of links, but of course there is no limit to what you can do with it.

To edit it, click `All` and edit `_sidebar`. From there, you can follow regular markdown syntax.

## Testing locally

Please, please, please test locally if you're new to markdown. That way, you avoid (for example) deleting pages by accident. You can easily test an identical (minus some pages perhaps) copy of the wiki locally by installing `php` with your distro's package manager and cloning [the repo](https://codeberg.org/speedie/speedwm-wiki) somewhere. If you don't know what you're doing, just cd into it, run `php -S localhost:1337` to start PHP, open any web browser and go to `localhost:1337`. Of course, the `1337` here can be swapped out for almost any value, it's just what I happen to go with.