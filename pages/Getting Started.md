Getting Started
---------------

spmenu is an X11 menu application which takes standard input, parses
it, and lets the user choose an option and sends the
selected option to standard output.

In addition to this, it also serves as a run launcher through the included
shell script `spmenu_run`, which handles both $PATH listing, .desktop entries
and file listing.

While spmenu is based on dmenu, and is also fully compatible with dmenu,
spmenu introduces many new features which can be useful in shell scripting.
There are way too many to list, but spmenu has a
[wiki](https://spmenu.speedie.site) which goes through features in more detail.

### Supported operating systems

This is not some arbitrary requirement. While it may run if it's not listed
here, I will not be providing support due to having a lack of experience with
said operating system. The main operating system that doesn't support
spmenu is Microsoft Windows. UNIX like operating systems are most likely to run
it. macOS has some different steps.

- Nearly all GNU/Linux distributions
  - NixOS and GNU Guix *may* require separate instructions, particularly
    when building. I will not provide support for these as of now,
    use the existing documentation to set it up.
    Chances are if you use one of these you already know how to compile
    software for it.
- FreeBSD, OpenBSD, NetBSD
  - There are likely more BSDs that can run speedwm, but I cannot support all
    of them. Try it and see if it works. If it doesn't please try and fix it.
    You know more about the BSDs than I do.
- macOS/Mac OS X
  - macOS *should* be supported, however it requires `Xquartz` to be installed
  on the system. See [[Using spmenu on macOS]] for more information.

### Installation on Gentoo GNU/Linux

**NOTE: The Gentoo overlay is as of now quite outdated and the spmenu-9999
ebuild is not functional. The latest stable (and functional) version available
is 0.3.2. Gentoo users are recommended to follow 'Installation on generic
GNU/Linux distributions'.**

Gentoo has slightly different instructions for setting up spmenu. While you
can follow along with the generic installation instructions, I have a Gentoo
overlay with packages for most if not all spmenu versions.

To get started, add [my overlay](https://git.speedie.site/speedie-overlay).
Then simply `emerge x11-misc/spmenu`.

If you want to install spmenu using Git, do this by adding the following
lines:

`=x11-misc/spmenu-9999 **`

to `/etc/portage/package.accept_keywords` and then emerging the packages as
per the instructions above.

### Installation on generic GNU/Linux distributions

Installing spmenu on most GNU/Linux distributions is very simple. First,
determine if you want to install it using Git (the absolute latest version) or
using a stable tarball.

While using a tarball can sometimes be more stable, it's not going to have any
bug fixes or features I'm adding. With Git, issues can be fixed rather often.
For this reason, it is recommended that you `git clone` the repository. If you
do not have `git`, install it using your distribution's package manager. If
you're going to be using a 'stable' tarball, you do not need to install `git`.

While the list of dependencies will vary depending on the spmenu version
you're going to install (see README.md for your specific version),
below is a list for the latest release.

- libX11
  - If you're using macOS, XQuartz is a dependency instead.
  - If you're using Wayland, `xorg-xwayland` is a dependency.
- libXrender
- freetype
- imlib2
  - Used for image support, can be disabled during compile time.
- libXinerama
  - Used for multi-monitor support, can be disabled during compile time.
- OpenSSL
  - Used to calculate MD5 of images if image support is enabled, can be
disabled during compile time.
- Pango
- Cairo
- libconfig
  - Can be disabled if you don't want/need config file support during compile time.
- meson
  - Used to compile spmenu, not optional unless you're experienced with build systems.

#### Installation using `git`

This assumes you have `git` installed.

`cd` into a location where your normal user (not root) has permission to both
read and write. Do not clone the spmenu repository in `~/.config` as spmenu
will write configuration files to that location.

From here on, we're going to assume you are in the location `~/Documents`. Run
`git clone https://git.speedie.site/spmenu spmenu` to clone the
repository into a directory named `spmenu`. Provided the repository hasn't
moved and you have a working internet connection, you should have received a
copy of the spmenu source code.

You can now `cd spmenu` into it so we can actually install it. If you need to
make changes to the source code for whatever reason, now you want to do that.

Now, it's time to compile it! Provided you have all dependencies installed,
this should be very, very easy and quick.

Configure the build by running these commands:

`mkdir -p build/ # Create a build/ directory, Meson will use this as the working
directory`

`meson setup build # This will check to make sure all dependencies are found.
If you're recompiling you may want to pass --reconfigure as an argument`

This is where you can enable/disable certain features, as well as set
compiler options.

Now, to build it run `ninja -C build`. If all went well you should have a
binary in the `build/` directory.

Finally, to install it all, run:

`meson install -C build --prefix /usr # /usr may be overriden to /usr/local
or anything else if you use macOS or simply want another destination directory`

To generate documentation, which may be necessary if you're pushing new changes
to your Git repository, run `scripts/make/generate-docs.sh` **in the current
directory**.

To generate a tarball, run `scripts/make/generate-pkg.sh` **in the current
directory**. If you want to generate a Pacman package, run
`scripts/make/generate-pacman-pkg.sh` instead. You can run `pacman -U` on this
if you wish later on.

Now follow along with 'Using spmenu'.

#### Installation using a tarball

Installation using a tarball works pretty much the same. Visit [the releases
page](https://ls.speedie.site/index.php?b=releases%2Fspmenu) and get the latest
 (or an old if you want) tarball.

`cd` into a location where your normal user (not root) has permission to both
read and write.

Now we need to unpack the tarball and get our source code. To do this, you can
use the `tar` command. I will spare you the time reading the man page and the
command to unpack the tarball is `tar -xpvf
/path/to/the/spmenu/tarball.tar.gz`. The `-x` argument here means extract.
`-p` means preserve permissions. The `-v` means verbose, we want to see
what's going on. `-f` means file, we want to specify a file to unpack.

After unpacking it, there should be one directory; `spmenu-<version>`. Let's
`cd spmenu-<version>` into it. Now it's time to compile spmenu!

Now, it's time to compile it! Provided you have all dependencies installed,
this should be very, very easy and quick.

Configure the build by running these commands:

`mkdir -p build/ # Create a build/ directory, Meson will use this as the working
directory`

`meson setup build # This will check to make sure all dependencies are found.
If you're recompiling you may want to pass --reconfigure as an argument`

This is where you can enable/disable certain features, as well as set
compiler options.

Now, to build it run `ninja -C build`. If all went well you should have a
binary in the `build/` directory.

Finally, to install it all, run:

`meson install -C build --prefix /usr # /usr may be overriden to /usr/local
or anything else if you use macOS or simply want another destination directory`

To generate documentation, which may be necessary if you're pushing new changes
to your Git repository, run `scripts/make/generate-docs.sh` **in the current
directory**.

To generate a tarball, run `scripts/make/generate-pkg.sh` **in the current
directory**. If you want to generate a Pacman package, run
`scripts/make/generate-pacman-pkg.sh` instead. You can run `pacman -U` on this
if you wish later on.

Now follow along with 'Using spmenu'.

### Using spmenu

spmenu accepts standard input. All of spmenu's features **require** standard input to be passed. If you only want to return the input text, you can simply pass nothing to spmenu and a blank menu will open up. The `spmenu_run` script bundled with spmenu which works as a run launcher uses this fact to list out all available executables which are then run. See the article [[Using spmenu as a run launcher]] for more information, this article **only** focuses on spmenu itself.

For example, `printf "Apple\nBanana\nOrange\n" | spmenu` will bring up a basic menu listing with three options, Apple, Banana and Orange. The one the user selects will be returned when (by default) Enter is pressed. You can also press Shift+Enter to select what you have typed in rather than the selected match.

spmenu also has a lot of arguments you can give. `spmenu --help` or if you prefer, `spmenu -h` will return a full list of available arguments. This is not the *only* way to configure spmenu, however it's useful when you want a setting to be applied only to the current instance. Most options are configurable through arguments, but not all of them are.

spmenu also has a config file through libconfig. A default config should be installed at `/usr/share/spmenu/spmenu.conf`. You can copy this to `~/.config/spmenu/spmenu.conf`. You may edit this file using your favorite text editor. Note that .Xresources takes priority over spmenu.conf configuration, as long as `xresources = 1`. You can find a list of accepted .Xresources values in `/usr/share/spmenu/example.Xresources`, and this file should more or less be identical to the defaults. For more information regarding the configuration file, see [[Configuring spmenu]].

Great, you should now know how spmenu works on a basic level. Now, if you are a tiling window manager user or just want a better run launcher, you can check out the article 'Using spmenu as a run launcher'. Alternatively, if you want to use this for scripting you can check out [[Scripting with spmenu]]. There are also many scripts that use spmenu to display items, see [[User scripts]] for that.

If you want to theme spmenu, you can see [[User themes]].
