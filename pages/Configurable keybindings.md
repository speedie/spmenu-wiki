Configurable keybindings
========================
spmenu has since version 1.0 supported configuring keybinds and mouse binds through a config file. The default config file *should* have the default keybinds already.
# List of valid modes
- -1
- Any mode
- 0
- Normal mode
- 1
- Insert mode
# List of valid clicks
- ClickWindow
- Clicking on the spmenu window
- ClickPrompt
- Clicking on the prompt
- ClickInput
- Clicking on the input
- ClickLArrow
- Clicking on the left arrow
- ClickRArrow
- Clicking on the right arrow
- ClickItem
- Clicking on the item area
- ClickSelItem
- Clicking on an item, function here doesn't matter, it will always be selected
- ClickNumber
- Clicking on the match count indicator
- ClickCaps
- Clicking on the caps lock indicator
- ClickMode
- Clicking on the mode indicator

# List of valid modifiers
- Ctrl
- Shift
- Super
- Alt
- AltGr
- ShiftGr
- None

These can also be combined by using '+' as a separator (ie. Ctrl+Shift)

# List of valid buttons

- Left Click
- Middle Click
- Right Click
- Scroll Up
- Scroll Down

# List of valid functions

**NOTE: This applies to both mouse and keybinds.**

- moveup
- movedown
- moveleft
- moveright
- moveend
- movestart
- movenext
- moveprev
- moveitem
- paste
- pastesel
- restoresel
- clear
- clearins
- viewhist
- moveword
- moveword
- deleteword
- movecursor
- movecursor
- navhistory
- navhistory
- backspace
- selectitem
- quit
- complete
- setimgsize
- setimgsize
- toggleimg
- togglefullimg
- defaultimg
- rotateimg
- flipimg
- setimgpos
- setimgpos
- setimggaps
- setimggaps
- setlines
- setlines
- setcolumns
- togglehighlight
- setprofile
- switchmode

# List of valid arguments

**NOTE: This applies to both mouse and keybinds.**

- 0
- 1 through 100
- +1 through +100
- -1 through -100

# List of valid modes

- -1
- Any mode
- 0
- Normal mode
- 1
- Insert mode

# List of valid keys

- None
- Space
- Enter
- Tab
- a
- b
- c
- d
- e
- f
- g
- h
- i
- j
- k
- l
- m
- n
- o
- p
- q
- r
- s
- t
- u
- v
- w
- x
- y
- z
- 0
- 1
- 2
- 3
- 4
- 5
- 6
- 7
- 8
- 9
- !
- "
- #
- $
- %
- &
- '
- (
- )
- *
- +
- ,
- -
- .
- /
- :
- ;
- <
- =
- >
- ?
- @
- [
- \\
- ]
- _
- grave
- {
- bar
- }
- ~
- F1
- F2
- F3
- F4
- F5
- F6
- F7
- F8
- F9
- F10
- F11
- F12
- PageUp
- PageDown
- Home
- End
- Delete
- PrintScr
- Esc
- Pause
- ScrollLock
- Backspace
- Up
- Down
- Left
- Right
- Next
- Prior
