Wayland support
---------------

As of now there are no plans to add Wayland support into spmenu. It is no secret that [I strongly dislike Wayland's ideas](https://speedie.site/articles/post56.php). That said, if someone wants to put in the work, it will be merged in. However not all hope is lost because for now and likely the very distant future, you'll be able to use spmenu on Wayland through XWayland, which is an X11 server running on Wayland.

spmenu has been tested on Hyprland using XWayland, and it works perfectly and feels like any native Wayland program. That said, please report any Wayland related issues you face.

If native Wayland support were to be added, it would require the following changes at a base minimum:

- Removal of .Xresources, at least on Wayland sessions
  - .Xresources is an X11 feature, and Wayland does not seem to have anything like it. Due to the libconfig configuration I don't see it being a huge issue though.
- Rewritten keybinds
  - Keybinds are done using libX11 which as the name implies is X11 specific. We would need to do this in some Wayland specific way, or at the very least find a library for handling keybinds which works on both protocols.
  - Maybe do wrapper functions for keybindings.
- Rewritten drawing
  - Drawing of everything will also have to be rewritten, because it all depends on X11 only libraries.
  - spmenu uses a modified version of suckless' libdrw. Because it's a separate library we could potentially handle it all in the same function with the same argument, leading to cleaner code.
  - I don't know how we detect if we're using an X11 or Wayland session
- Rewritten fonts
  - Pango markup works fine, but currently spmenu uses libXft which is an X11 specific library and will not work .
  - Cairo would be a good fit if we swap libXft out entirely. libXft has a long history of issues, so it would be a benefit on the X11 side too.
- Rewritten client creation and resizing
  - Finally we would have to rewrite client creation and resizing.
  - libXinerama would have to be replaced with something else for the Wayland side.
- Support both Wayland and X11
  - I think it is important to support both X11 and Wayland, or at the very least decide on one during compile time and providing multiple packages (ie. `spmenu-x11` and `spmenu-wayland`).